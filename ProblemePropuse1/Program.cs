﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemePropuse1
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b;
            double x = 0;
            Console.WriteLine("Introduceti a: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduceti b: ");
            b = int.Parse(Console.ReadLine());
            if (a == 0)
                Console.WriteLine("Ecuatia nu are solutie");
            else
                x = -b / a;
            Console.WriteLine("Solutia ecuatiei " + a + "x" +" + "+b+" = 0 este: x = " + x);
            Console.ReadKey();
        }
                 
    }

}
